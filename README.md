# Formation CSS

## Utiliser

Ce support utilise [revealJS](https://revealjs.com/) et [reveal-md](https://github.com/webpro/reveal-md).

Cloner le dépôt, installer le paquet reveal-md avec npm, puis le lancer.

```
git clone 
npm install
npm start
```

## Générer le pdf

```
npm run build:pdf
```

[CI par jrmi](https://framagit.org/jrmi/cours-fullstack)
