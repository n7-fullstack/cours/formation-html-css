# CSS Filage

## Séance 1 : CSS (1 journée)

- Codepen ?
- Utiliser les mêmes noms de classe

### Pré-requis

- Faire le tuto HTML/CSS sur mozilla
- Lire une page du W3C (à choisir au choix)
- la veille : lire un cheatsheet CSS

### Déroulé

- Intro : 15min
- Rappels CSS Bases, sélecteurs, propriétés, unités : 30min

#### Mise en forme

- Mise en forme, couleurs, bordures, fonds, polices : 20min
- TP mise en forme : bannière, arrondi, couleurs et bordures : 40min
- Pause : 10min

#### Mise en page

- Style des blocs et modèle de boîte
- Positionnement par rapport au flux HTML
- Modifier le comportement d'un élément avec `display`
- Mise en page, positionnement, disposition des éléments dans la page : 30min
    - Flexbox
    - Grid
- Atelier mise en page : 30min
- Atelier menu et galerie : 40min

#### Responsive

- Responsive : mediaqueries et navigateur

#### Frameworks CSS

- Bootstrap
- Material
- Tailwind
- Blueprint

### Post séance

- Jeu hiérarchie des sélecteurs [flukeout.github.io](http://flukeout.github.io/)
- jeu flexbox
- jeu grid

## Séance 3 : CSS notions avancée

- Inclure une police
- Animations et transitions
- Trucs & Astuces : marges négatives, 
- Bonnes pratiques (id vs class, fond et forme, mobile first)
- Framework CSS
- SASS
