# HTML Filage

## Séance 1 : HTML bases (1/2 journée)

- durée : 3h30

### Pré-requis

- Faire le tuto HTML/CSS sur mozilla
- Lire un article (à choisir parmi 4 articles à définir)
- la veille : lire un cheatsheet HTML

### Déroulé

- Présentations / tour de prénoms : 10min
- Activité : Le débuggeur : 20min
- HTML bases : 20min
- Atelier créer ma première page, le CV : 20min
- Pause : 10min
- Medias : 20min
- Atelier insérer des media dans ma page, la photo de profil : 20min
- Formulaires : 20min
- Atelier créer un formulaire de contact : 10min

### Post séance

- [deboggage de code HTML](https://developer.mozilla.org/fr/docs/Apprendre/HTML/Introduction_%C3%A0_HTML/Debugging_HTML#Apprentissage_actif_%C3%A9tude_avec_un_code_permissif)

### Resources

- [Atelier structure page web](https://developer.mozilla.org/fr/docs/Apprendre/HTML/Introduction_%C3%A0_HTML/Structuring_a_page_of_content)
