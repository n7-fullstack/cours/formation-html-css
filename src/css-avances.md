---
title: CSS avancés
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

<!-- .slide: data-background="#333" -->

# CSS avancées

## (partie 2)

## octobre 2023

<!-- v -->

## Sommaire

- Sélecteurs
- Mise en forme : inclure une police, transitions…
- Positionnement : Flexbox, Grid
- Responsive : mediaqueries et navigateur
- Accessibilité
- Préprocesseur SASS
- Framework CSS

<!-- v -->

## Ressources

- Documentation CSS : https://developer.mozilla.org/fr/docs/Web/CSS
- Blog CSS Tricks : https://css-tricks.com/
- Can I use : https://www.caniuse.com/

<!-- s -->

<!-- .slide: data-background="#333" -->

# Rappels

<!-- v -->

## La syntaxe

![Syntaxe CSS (Mozilla Dev Network)](./assets/images/CSS.svg)

<!-- v -->

## Ajouter du style

* fichier lié

    ```html
    <link rel="stylesheet" href="styles.css" type="text/css" />
    ```

* entre les balises `<style>` dans la partie `<head>`
* dans la balise directement avec l'attribut `style` (déconseillé)

<!-- v -->

### Sélecteurs de balise

```css
h1 { color: red; }
```

### Sélecteurs de classe

```css
.title { color: blue; }
```

### Sélecteurs d'id

```css
#main { color: green; }
```

<!-- v -->

## Les unités

* absolues : `px`, `pt`
* relatives : `em`, `rem` (root em), relative à la taille de police de l'élément racine (`<html>`)
* `%`, relative à l'élément parent, sa taille de police ou ses dimensions

<!-- v -->

## Modèle de boîte

### Les éléments

![Box model](assets/images/box-model.png)

<!-- v -->

### Modèle standard

![Box model](assets/images/standard-box-model.png)

```css
{
    box-sizing: border-content;
}
```

<!-- v -->

### Modèle alternatif

![Box model](assets/images/alternate-box-model.png)

```css
{
    box-sizing: border-box;
}
```

<!-- v -->

## Comportements

* `block` → les uns sous les autres, occupe tout l'espace en largeur
* `inline`, `inline-block` → dans le flux du texte en ligne
* `flex` → les éléments enfants directs sont flexibles soit en ligne soit en colonne
* `grid` → alignement des éléments enfants en ligne et colonne

<!-- v -->

Redéfinir le comportement

```css
{
    display: block; /* | inline | inline-block … */
}
```

<!-- v -->

## Positionnement / flux HTML

- `static` : par défaut
- `absolute` : hors du flux
- `relative` : reste dans le flux
- `fixed` : hors du flux, reste dans la partie visible

![Positionnement absolu](./assets/images/positionnement-absolute.png)

<!-- v -->

## Élément flottant

```css
img {
    float: left; /* right | none */
}
```

![Float left](./assets/images/float-image-left.png)

<!-- v -->

## Flexbox

```html
<div class="flex-container">
    <div>block 1</div>
    <div>block 2</div>
</div>
```

```css
.flex-container {
    display: flex;
}
```

![Flexbox](./assets/images/flexbox.svg)

<!-- s -->

<!-- .slide: data-background="#333" -->

# Sélecteurs avancés

<!-- v -->

* Par attribut

    ```css
    a[attribut~="valeur"]
    ```

* Par élément fils direct

    ```css
    a > b
    ```

* Par élément frère

    ```css
    a + b
    ```

<!-- v -->

* Par fils premier, dernier ou tous les x éléments

    ```css
    a:first-child
    a:last-child
    a:nth-child(expression)
    ```

* première lettre, première ligne

    ```css
    ::first-letter
    ::first-line
    ```

* négation

    ```css
    a:not(.class)
    ```
<!-- v -->


## Hiérarchie des styles

Vers le plus important

- Implémentation du navigateur
- \> Styles <link ...>
- \> Styles définis dans <styles> dans la page HTML
- \> style défini dans la balise

<!-- v -->

## Hiérarchie de sélecteurs

sélecteur de classe > Sélecteur de balise > Sélecteur d'id

<!-- v -->

<!-- .slide: data-background="#333" -->

## Exercice

Déroulez le tutoriel [flukeout.github.io](http://flukeout.github.io/)

<!-- s -->

<!-- .slide: data-background="#333" -->

# Unités

<!-- v -->

## Absolues

* `px` : pour les supports écran
* `pt` : pour les supports imprimés

<!-- v -->

## Relatives

* `em`, `%`
* `rem` (root em), relative à la taille attibuée au document

<!-- v -->

### Bonne pratique

Définir la taille de référence pour le `<body>`, puis définir les autres tailles en `em` ou `rem`

<!-- v -->

## Viewport

* `vh` et `vw`
* overflow

Un bloc qui fait toute la hauteur visible

```css
.map {
    height: 100vh;
}
```

<!-- v -->

## calc()

Calculer une dimension

```css
#main {
    height: calc(100vh - 50px);
```

<!-- v -->

### Cas d'usage

```css
#header {
    height: 50px;
}
#main {
    height: calc(100vh - 50px);
    overflow-y: auto;
}
```

<!-- v -->

![Viewport et calc](./assets/images/viewport.png)

<!-- s -->

<!-- .slide: data-background="#333" -->

# Mise en forme CSS3

<!-- v -->

## Propriétés CSS3

* `background-size`, `border-radius`, `opacity`,
* `box-shadow` : [exemples](https://codepen.io/ericbutler555/pen/ogJdMg) [sur codepen](https://codepen.io/thomasjwicker/pen/jzbHt)
* dégradés
* arrière-plan multiples
* transparence pour la couleur de fond

[css3generator.com](https://css3generator.com/)

<!-- v -->

## Transparence

```css
div {
    background-color: rgba(100, 120, 255, 0.5);
}
```

<!-- v -->

## Polices de caractère spéciales

### Inclure une police

- Ajouter les fichiers dans le dossier `/fonts/` de votre site web
- Déclarer les polices

    ```css
    @font-face {
        font-family: "Roboto";
        src: url("/fonts/Roboto-Bold.woff2") format("woff2"),
            url("/fonts/Roboto-Bold.ttf") format("ttf"),
            url("/fonts/Roboto-Bold.woff") format("woff");
        font-style: normal;
        font-weight: bold;
    }
    ```

<!-- v -->

Pour utiliser la police

```css
h1, h2 {
    font-family: Roboto, sans-serif;
}
```

<!-- v -->

### Types de police acceptés

* TrueType  `font/ttf`
* OpenType `font/otf`
* Web Open File Format `font/woff`
* Web Open File Format 2 `font/woff2`

Documentation [@font-face sur MDN web dev](https://developer.mozilla.org/fr/docs/Web/CSS/@font-face)

<!-- v -->

### Trouver des polices

* https://www.fontsquirrel.com : trouver des polices pour le web, nombreuses librement réutilisables
* https://fonts.google.com : facile, mais ajoute un lien externe à votre site (trackers de pub)

<!-- v -->

## Transitions

Transition entre deux états d'un élément (`:hover` ou `:active` ou action javascript)

```css
transition: margin-right 2s, color 1s;
```

Voir sur Mozilla web dev :

* [documentation](https://developer.mozilla.org/fr/docs/Web/CSS/transition)
* [utilisation](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Transitions/Utiliser_transitions_CSS)

<!-- v -->

### Transitions en détail

* `transition-property` : propriété sur laquelle elle s'applique (`all`, `margin`, `width`, `color`…)
* `transition-duration` : durée (secondes ou milli-secondes)
* `transition-delay` : délai du déclenchement
* `transition-timing-function` : fonction de temps

<!-- v -->

## Transformations

Modifie l'espace de coordonnées utilisé pour la mise en forme visuelle

```css
transform: scale(120%) rotate(20deg);
```

Voir sur Mozilla web dev

* [documentation](https://developer.mozilla.org/fr/docs/Web/CSS/transform)
* [utilisation](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Transforms/Utilisation_des_transformations_CSS)

<!-- v -->

### Fonctions de transformation

* translation `translate()`
* rotation `rotate()`
* homothétie `scale()`
* déformer `skew()`
* changer la perspective `perspective()`

<!-- s -->

<!-- .slide: data-background="#333" -->

# TP / session 1 : <br>Mise en forme

Télécharger le [TP](https://gitlab.com/n7-fullstack/cours/tp-css-site-artisanat/-/releases/tp2-v1.0) et le dézipper

Énoncé disponible sur https://gitlab.com/n7-fullstack/cours/tp-css-site-artisanat/-/blob/tp2-v1.0/TP2-TODO.md

Notes:

*Ou bien*

* Cloner le dépôt<br>
<small>`git clone git@gitlab.com:n7-fullstack/cours/tp-css-site-artisanat.git` depuis [tp-css-site-artisanat](https://gitlab.com/n7-fullstack/cours/tp-css-site-artisanat)</small>
* Se positionner sur le tag tp2-v1.0<br>
<small>`git checkout tp2-v1.0`</small>

<!-- s -->

<!-- .slide: data-background="#333" -->

# Mise en page en CSS3

<!-- v -->

## Flexbox

```css
.flex {
    display: flex;
}
```

```html
<div class="flex">
    <div>texte 1</div>
    <div>texte 2</div>
</div>
```

<!-- v -->

### Exemple 1 : barre de navigation

Aligner les éléments de la barre de navigation

```html
<ul class="nav">
    <li>item 1</li>
    <li>item 2</li>
    <li>item 3</li>
</ul>
```

```css
.nav {
    display: flex;
    align-items: flex-end;
    list-style: none;
}
```

<!-- v

### Exemple 2 : aligner des blocs
-->

<!-- v -->

### Exemple 3 : contenu de blocs media + texte

```html
<div class="product-list">
  <article>
    <h2>Titre</h2>
    <img src="">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    <a class="btn" href="#" %}="">Détails</a>
  </article>
  <article>
    …
  </article>
</div>
```

```css
.product-list {
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
}
.product-list article {
  flex-basis: 48%;
  padding: 20px;
  margin: 20px 0;
  background-color: #FFF;
  box-shadow: 2px 2px 5px #CCC;
  border-radius: 5px;
  display: flex;
  flex-direction: column;
}
```
<!-- v -->

### Ressources

* [Tutoriel sur MDN web docs](https://developer.mozilla.org/fr/docs/Apprendre/CSS/CSS_layout/Flexbox)
* [Guide sur Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
* [Sur Alsacreations](https://www.alsacreations.com/tuto/lire/1493-CSS3-Flexbox-Layout-module.html)

[À vous de jouer !](https://flexboxfroggy.com/#fr)

<!-- v -->

## Grid layout

<!-- v

### Exemple : grille de page
 -->

<!-- v -->

### Ressources

* [Tutoriel sur MDN web docs](https://developer.mozilla.org/fr/docs/Apprendre/CSS/CSS_layout/Grids)
* [Guide Grid layout sur CSS Tricks](https://css-tricks.com/snippets/css/complete-guide-grid/)
* [Sur Alsacreations](https://www.alsacreations.com/article/lire/1388-CSS3-Grid-Layout.html)

[À vous de jouer !](https://cssgridgarden.com/#fr)

<!-- v -->

## Flexbox ou Grid layout ?

* Grid Layout : emplacements principaux
* Flexbox : contenus et composants internes

<!-- v -->

![Flexbox ou Grid layout](./assets/images/alsacreation_flex_grid.png)

*Source : [Alsacreation](
https://www.alsacreations.com/article/lire/1794-flexbox-ou-grid-layout.html)*

<!-- s -->

<!-- .slide: data-background="#333" -->

# TP / session 2 : <br>Mise en page

Disponible sur https://gitlab.com/n7-fullstack/cours/tp-css-site-artisanat/-/blob/tp2-v1.0/TP2-TODO.md

<!-- s -->

<!-- .slide: data-background="#333" -->

# Responsive design

<!-- v -->

## Introduction

Quantité de modèles de smartphones, de version de Android, IOS ou Windows Phone, de navigateurs et leurs versions, surchargés par les opérateurs ...
Impossible de tout couvrir.

<!-- v -->

### Organisation et contenu responsive

* Organisation responsive : disposition varie en fonction de la définition d'écran
* Contenu responsive : certains contenus peuvent être affichés ou non selon la définition d'écran

<!-- v -->

## Media-queries

```css
@media (min-width: 700px) and (orientation: landscape) {…}
```

* Orientation (`portrait` ou `landscape`) et Localisation
* Device api (`screen`, `print`, `tv`, )

Importance de définir des points d'arrêt pertinents en fonction des terminaux les plus utilisés.

<!-- v -->

### Balise meta viewport

```html
<meta
    name="viewport"
    content="width=device-width,initial-scale=1">
```

<!-- v -->

## Approche mobile first

```css
.post-image {
    max-width: 100%;
}
@media (min-width: 700px) {
    .post-image {
        width: 360px;
        margin-right: 15px;
        float: left;
    }
}
@media (min-width: 1040px) {
    .post-image {
        width: 520px;
    }
}
```


<!-- v -->

![Responsive](./assets/images/responsive.png)

<!-- s -->

<!-- .slide: data-background="#333" -->

# TP / Session 3 : <br>Mediaqueries

<!-- s -->

<!-- .slide: data-background="#333" -->

# Accessibilité

<!-- v -->

## Pour qui ?

> <small>Le handicap est défini comme : "toute limitation d’activité ou restriction de participation à la vie en société subie dans son environnement par une personne en raison d’une altération substantielle, durable ou définitive d’une ou plusieurs fonctions physiques, sensorielles, mentales, cognitives ou psychiques, d’un polyhandicap ou d’un trouble de santé invalidant"</small>

<small>– (article L. 114 du code de l’action sociale et des familles)</small>

<!-- v -->

## Les réglements

### Le W3C

- groupe de travail [Web Accessibility Initiative](https://www.w3.org/WAI/) (WAI)
- standards d'accessibilité [Règles pour l’accessibilité des contenus Web](https://www.w3.org/WAI/standards-guidelines/fr) (WCAG v2.1)

<!-- v -->

### En France

[Référentiel Général d'Amélioration de l'Accessibilité](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/) (RGAA v4)

- WCAG v2.1 dans le droit français
- Obligation sur les sites d'administration, leurs sous-traitants, sites de grandes entreprises
- Niveau (A) et (AA) obligatoires

<!-- v -->

## Des outils

- [Checklists OpQuast](https://checklists.opquast.com/fr/) sur les standards du web dont l'accessibilité
- [Access42](https://access42.net/decouvrir-accessibilite), entreprise spécialisée

<!-- v -->

## Dans la pratique

- respecter les standards du web
- rendre toutes les fonctionnalités accessible au clavier seul
- faire attention au contraste de couleurs
- identifier visiblement les liens (pas seulement par la couleur)
- ajouter des sous-titres pour les medias et description pour les images
- utiliser des [rôles ARIA](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques) pour les éléments dynamiques

<!-- s -->

<!-- .slide: data-background="#333" -->

# Merci !
