---
title: Frameworks CSS
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

<!-- .slide: data-background="#333" -->

# Frameworks CSS

## 1 décembre 2023

<!-- v -->

## Sommaire

- Preprocess Sass
- Framework CSS Bootstrap
- Framework CSS Tailwind

<!-- v -->

## Ressources

- Documentation CSS : https://developer.mozilla.org/fr/docs/Web/CSS
- Blog CSS Tricks : https://css-tricks.com/
- Can I use : https://www.caniuse.com/
- State of CSS : Frameworks CSS

<!-- s -->

<!-- .slide: data-background="#333" -->

# SASS

<!-- v -->

* Avantages
* Variables
* Imbrication de code
* Mixins
* Importation de fichier
* Installation et utilisation

<!-- v -->

## Installer [Sass](http://www.sass-lang.com/)

En utilisant npm

```
npm install -g node-sass
```

<!-- v -->

## Les bases

### Variables

```css
$font-stack: Helvetica, sans-serif;
$primary-color: #333;

body {
  font: 100% $font-stack;
  color: $primary-color;
}
```

Résultat :

```css
body {
  font: 100% Helvetica, sans-serif;
  color: #333; }
```

<!-- v -->

### Imbrication de code

```css
nav {
  ul {
    list-style: none;

    >li {
      display: inline-block;
    }
  }
}
```

Résultat :

```css
nav ul {
  list-style: none; }
  nav ul > li {
    display: inline-block; }
```

<!-- v -->

## Mixins et fonctions

```css
@red {
    color: red;
    border: 1px solid red;
}
a {
    background: blue;
    @include red;
}

b {
    background: white;
    @include red;
}
```

<!-- v -->

Résultat :

```css
a {
    background: blue;
    color: red;
    border: 1px solid red;
}

b {
    background: white;
    color: red;
    border: 1px solid red;
}
```

<!-- v -->

## Import de fichier

```css
@import "./chemin/vers/fichier";
```


<!-- s -->

<!-- .slide: data-background="#333" -->

# Frameworks CSS

<!-- v -->

## C'est quoi un framework ?

Feuilles de styles (voire composants JS) prêtes à l'emploi pour intégrer rapidement un design.

Les plus connus :

- Bootstrap
- Materialize CSS (Google)
- Tailwind
- PureCSS
- …

Source : [Quels framework et méthodologie CSS choisir ?](https://www.alsacreations.com/actu/lire/1823-quels-framework-methodologie-css-choisir.html) sur Alsacréations (2021)

<!-- v -->

### On y trouve

* Règles de "reset", normaliser les styles par défaut des navigateurs
* Un système de grille, disposition des éléments
* Éléments de typographie, tailles de police, espacement, formulaires
* Classes prédéfinies pour construire rapidement des composants (bloc media + texte, barres de navigation)

<!-- v -->

## Sans UI

Seulement le "reset" et le système de grille : très légers

* [SimpleGrid](https://simplegrid.io/)
* [KNACSS](https://www.knacss.com/) par Raphaël Goetter et AlsaCreations
* [PureCSS](https://purecss.io/)

<!-- v -->

## Avec UI et composants animés

Beaucoup plus complets, mais aussi plus lourds.

* [Bootstrap](https://getbootstrap.com/)
* [Tailwind](https://tailwindcss.com/)
* [Materialize CSS](https://materializecss.com/)
* [Foundation](https://get.foundation/)
* [Bulma](https://bulma.io/)

<!-- v -->

## Lequel utiliser ?

Ça dépend du projet :)

Usage des CSS Framework sur State of CSS

https://2023.stateofcss.com/fr-FR/css-frameworks/

<!-- s -->

<!-- .slide: data-background="#333" -->

# Bootstrap 5

<!-- v -->

## Les principaux composants

* Mise en forme CSS : typographie, images, tables…
* Grille de mise en page, basé sur Flexbox
* Composants : boutons, navigation, "cards", pagination…
* Utilitaires

[Des exemples](https://getbootstrap.com/docs/5.3/examples/)

Les frameworks CSS ont globalement cette structure

<!-- v -->

##  Mise en forme CSS

[Content](https://getbootstrap.com/docs/5.3/content/typography/)

* Typography (alignement, blockquotes, lists)
* Tables
* Images

<!-- v -->

##  La grille bootstrap

[Grille](https://getbootstrap.com/docs/5.3/layout/grid/)

* Le principe
* colonnes imbriquées
* offset
* ordre des colonnes

<!-- v -->

##  Formulaires

[Formulaires](https://getbootstrap.com/docs/5.3/forms/overview/)

<!-- v -->

##  Composants

[Composants](https://getbootstrap.com/docs/5.3/components/)

* Icons
* Boutons
* Menus déroulants
* Navigation
* Breadcrumb, pagination, label, badges
* Media (groupe d'image + texte)
* Vignettes

<!-- v -->

## TP framework : Intégrer un design simple

Reprendre le TP et importer le framework Bootstrap

1.  Utilisez les classes Bootstrap pour mettre en oeuvre la grille
2.  Utiliser le HTML de Bootstrap pour les éléments media, rendre les
    vignettes arrondies

<!-- s -->

<!-- .slide: data-background="#333" -->

# Tailwind CSS

<!-- v -->

## Une logique très différente

* Nom de classes uniquement utilitaires
* Haute personnalisation
* Requiert postCSS pour générer les CSS
* Outils permettant de créer ses propres classes

Lire l'article [Tailwind CSS, découverte du framework original et innovant](https://www.alsacreations.com/tuto/lire/1812-Tailwind-CSS-decouverte-du-framework-original-et-innovant.html) Alsacréations (2020)

<!-- v -->

## Pour commencer

1. Installation via npm

    ```bash
    npm install tailwindcss
    ```

2. Créer un fichier source `src/styles.css`
3. Importer tailwind
    ```css
    /* styles.css */
    @tailwind base;
    @tailwind components;
    @tailwind utilities;
    ```

4. Générer le fichier compilé

    ```bash
    npx tailwindcss build src/styles.css -o assets/css/styles.css
    ```

<!-- v -->

## TP : utiliser Tailwind dans notre projet

Mettez les classes nécessaires pour votre projet.

<!-- s -->

<!-- .slide: data-background="#333" -->

# Merci !
