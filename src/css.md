---
title: CSS
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

<!-- .slide: data-background="#333" -->

# CSS : habiller le web (partie 1)

## N7 Fullstack
## octobre 2023

<!-- v -->

## Le plan

<!-- v -->

- Bases : propriétés, valeurs, sélecteurs, unités
- Mise en forme, couleurs, bordures, fonds, polices
- Style des blocs et modèle de boîte
- Mise en page
- Positionnement par rapport au flux HTML
- Modifier le comportement d'un élément avec `display`
- Flexbox
- Responsive et mediaqueries
- Frameworks CSS
- Méthodologie et bonnes pratiques

<!-- v -->

## Les supports et les exos

Consulter le cours [sur gitlab](https://gitlab.com/n7-fullstack/cours/formation-html-css/-/blob/master/src/css.md)

TP : votre CV version web

Ressource [Atelier web ESAD](https://ateliers.esad-pyrenees.fr/web/pages/projets/cv/)

<!-- s -->

<!-- .slide: data-background="#333" -->

# Les bases en CSS

<!-- v -->

## La syntaxe

![Syntaxe CSS (Mozilla Dev Network)](./assets/images/CSS.svg)

```css
selecteur {
    propriété: valeur[unité];
}
```

<!-- v -->

Exemple

```css
h1 {
    color: green;
    font-size: 48px;
    border-bottom: blue 1px solid;
}
h1, p, li {
    color: red;
}
/* Commentaires
sur plusieurs lignes
*/
```

<!-- v -->

## Ajouter du style

* fichier lié

    ```html
    <link rel="stylesheet" href="styles.css" type="text/css" />
    ```

* entre les balises `<style>` dans la partie `<head>`
* dans la balise directement avec l'attribut `style` (déconseillé)

Le navigateur applique également des styles par défaut

<!-- v -->

## En pratique

Créez un fichier html `index.html`

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Plateforme</title>
  </head>

  <body>
    <h1>Titre 1</h1>
    <p>Bienvenue dans ce cours de CSS</p>
  </body>
</html>
```

Ouvrez-le dans le navigateur, et ouvrez le debugger (`ctrl + F12`)

<!-- v -->

### Entre les balises `<style>`

```html
<!doctype html>
<html>
    <head>
        <style>
        h1 { color: red; }
        </style>
    </head>
    <body>
        <h1>Les CSS</h1>
    </body>
</html>
```

<!-- v -->

### Fichier lié

```html
<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="./styles.css" />
    </head>
    <body>
        <h1>Les CSS</h1>
    </body>
</html>
```

Fichier `styles.css`

```css
h1 { color: green; }
```

<!-- v -->

### Dans la balise directement


```html
<!doctype html>
<html>
    <head>
    </head>
    <body>
        <h1 style="color: blue;">Les CSS</h1>
    </body>
</html>
```

<!-- v -->

### En pratique

* Appliquer les trois styles à la fois : de quelle couleur sera le titre ?

* Visualiser dans l'inspecteur de code

Note:

Si vous inversez la place du `<link …>` et de `<style>`, la couleur du titre sera différente.

<!-- v -->

## Hiérarchie des styles

**Du plus proche au plus éloigné de la balise**

<!-- s -->

<!-- .slide: data-background="#333" -->

# Les sélecteurs

<!-- v -->

## Les usuels

```html
<h1 class="title" id="main">Les CSS</h1>
<h2 class="title article">Les sélecteurs</h1>
```

- Plusieurs classes possibles
- Un seul id possible, unique dans la page

<!-- v -->

### Sélecteurs de balise

```css
h1 { color: red; }
```

### Sélecteurs de classe

```css
.title { color: blue; }
```
Cible tous les éléments ayant pour classe `title`

```css
h1.title { color: blue; }
```
Cible les élément de type `h1` ayant pour classe `title`

<!-- v -->

### Sélecteurs d'id

```css
#main { color: green; }
```

- Ne doit apparaître qu'une fois dans le code HTML
- À réserver pour identifier un contenu (`#header`, `#main-title`)

<!-- v -->

## Hiérarchie des sélecteurs

Du plus fort au moins fort

- Sélecteur d'id
- Sélecteur de classe
- Sélecteur de balise

<!-- v -->

## Combinaisons de sélecteurs

### Plusieurs sélecteurs

```css
h1, h2 { color: red; }
```

```css
p, .para, #chapo { color: grey; }
```

### Sélecteurs en cascade

```css
.header h1 { color: blue; }
```

Cible `h1` situé dans un élément de classe `header`

<!-- v -->

## Autres sélecteurs

### Sélecteur universel

```css
* { … }
.header * { …  }
```

<!-- v -->

### Sélecteurs suivant

```css
h1 + p {
    font-size: 150%;
}
```

### Sélecteurs enfant direct

Sélectionner uniquement les éléments directs

```css
ul > li {
    …
}
```

<!-- v -->

### Pseudo-classe

Exemple, un lien au survol

```css
a:hover {
    text-decoration: overline;
}
```

S'applique à n'importe quel élément.

*D'autres pseudo-classes existent → CSS avancées*

<!-- s -->

<!-- .slide: data-background="#333" -->

# Propriétés et valeurs

<!-- v -->

## Valeurs numériques

```css
{
    width: 300px;
    height: 2em;
    font-size: 200%;
    line-height: 1.2;
}
```

## Chemin ou URL

```css
{
    background-image: url('chemin/vers/image');
    background-image: url('https://exemple.com/lien/vers/image');
}
```

<!-- v -->

## Mots-clé

Spécifiques à la propriété

```css
{
    border-style: solid;
    position: absolute;
    text-decoration: none; /* annule une propriété */
}
```

<!-- v -->

## Série de valeurs

```css
{
    border: 1px solid red;
}
```

Équivaut à

```css
{
    border-width: 1px;
    border-style: solid;
    border-color: red;
}
```

<!-- v -->

## La couleur

### Texte, fond, bordure

```css
{
    color: #446655;
    background-color: #F0F7F3;
    border-color: red;
}
```

<!-- v -->

### Notation des couleurs

Nom ou code RGB (Red, Green, Blue) en hexadécimal

```css
{
    color: purple;
    color: #800080;
    color: #CCCCCC;
    color: #CCC; /* Forme simplifiée */
}
```

Code RGB et HSL (Hue, Saturation, Luminosity) en décimal, de 0 à 255

```css
{
    color: rgb(128, 0, 128);
    color: hsl(300, 100%, 25%);
}
```

*Transparence possible → CSS avancées*

<!-- s -->

<!-- .slide: data-background="#333" -->

# Formater du texte

<!-- v -->

## Polices de caractère

Famille de police, taille, style et niveau de gras

```css
{
    font-family: Verdana, sans-serif;
    font-size: 16px;
    font-style: italic; /* normal | italic | oblique */
    font-weight: bold; /* light | normal | medium | bold | 100-900 */
}
```

<!-- v -->

### Familles de police

* `serif`
* `sans-serif`
* `monospace`
* *`cursive` et `fantasy`, peu utilisées*

<!-- v -->

### Polices valides pour le web

Famille de police du web valides pour le web :

- Arial, Verdana pour les sans-serif
- Georgie et Times New Roman pour les serif
- Courrier New pour monospace

Préciser la famille de police à appliquer si la police n'est pas disponible

```css
{
    font-family: Arial, Lucida, Verdana, sans-serif;
}
```

*Ajouter nos propres polices → CSS avancées*

<!-- v -->

## Casse et décoration du texte

N'écrivez pas en majuscules… tranformez en CSS !

```css
{
    text-transform: uppercase /* capitalize | lowercase | none */;
    text-decoration: underline /* overling | line-through | none */;
}
```

<!-- v -->

## Alignement et hauteur du texte

```css
{
    text-align: justify /* left | center | right */;
    line-height: 1.25 /* 125% | 1.25em | 20px */;
}
```

<!-- v -->

## Les unités

Unités les plus courantes

### Absolues

* `px` : pixels, pour les supports écran
* `pt` : points, pour les supports imprimés

<!-- v -->

### Relatives

* `em`, relative à la taille de police de l'élément parent
* `rem` (root em), relative à la taille de police de l'élément racine (`<html>`)
* `%`, relative à l'élément parent, sa taille de police ou ses dimensions

*D'autres unités existent → CSS avancées*

<!-- v -->

#### Différence entre `em` et `%`

* `em`, relative à la taille de police de l'élément parent
* `%`
    - si s'applique à la taille de police, pourcentage de la taille de police de l'élément parent
    - sinon, pourcentage des dimensions de l'élément parent

<!-- v -->

### Bonne pratique

Définir la taille de référence pour le `<body>`, puis définir les autres tailles en `em` ou `rem`

<!-- v -->

## Les liens

Particularité : accès aux comportements de survol, focus ou visité par des **pseudo-classes**

```css
a:hover {
    text-decoration: none;
}
a:focus {
    border: 1px solid red;
}
a:visited {
    color: #666;
}
```

On peut appliquer ces pseudo-classes sur d'autres éléments que les liens

<!-- s -->

<!-- .slide: data-background="#333" -->

# TP → Mise en forme

## Ajout de la feuille de styles

* Lier une feuille de style commune à toutes les pages

<!-- v -->

<!-- .slide: data-background="#333" -->

## Mise en forme du texte et des liens

1. Définir une police sans serif pour tout le texte
1. Couleur `#333` pour toute la page
1. Appliquer la couleur `#b95b03` pour les lien et `#ff7f08` au survol
1. Appliquer une couleur sombre pour la section compétences

<!-- s -->

<!-- .slide: data-background="#333" -->

# Styles des blocs

<!-- v -->

## Modèle de boîte

### Les éléments

![Box model](assets/images/box-model.png)

<!-- v -->

### Modèle standard

![Box model](assets/images/standard-box-model.png)

- Largeur totale = width + padding + border left et right
- Hauteur totale = height + padding + border top et bottom

```css
box-sizing: border-content;
```

<!-- v -->

### Modèle alternatif

![Box model](assets/images/alternate-box-model.png)

Largeur / hauteur totale = largeur / hauteur définie (padding inclus)

```css
box-sizing: border-box;
```

<!-- v -->

## Comportements

* `block` → les uns sous les autres, occupe tout l'espace en largeur
* `inline` ou `inline-block` → dans le flux du texte en ligne

`inline-block` ? on peut définir des marges et les dimensions, alors que sont sans effet pour `inline`

<!-- v -->

### Balises génériques

* `<div>` → block
* `<span>` → inline

<!-- v -->

### Spécifiques aux tableaux

* table : `<table>`
* table-row : `<tr>` ou `<th>`
* table-cell : `<td>`

<!-- v -->

### Introduits en CSS3

* flex
* grid

<!-- v -->

### Redéfinir le comportement

```css
{
    display: block; /* | inline | inline-block … */
}
```

Exemple

```css
a {
    display: block;
}
p {
    display: inline;
}
```

<!-- v -->

## Largeur et hauteur

```css
{
    width: 300px;
    height: 150px;
}
```

En pourcentage, relative à la largeur ou la hauteur de l'élément parent

```css
{
    width: 40%;
    height: 80%;
}
```

<!-- v -->

## Marges et espacement

```css
{
    margin: 10px;
    padding: 10px;
}
```

Les marges sont sans effet sur les éléments `inline` (`strong` ou `a` par exemple)

<!-- v -->

### Par côté

```css
{
    margin-bottom: 10px;
    padding-left: 10px;
}
```

<!-- v -->

### Combiné

`margin: <top> <right> <bottom> <left>`
`margin: <top & bottom> <right & left>`

Exemple

```css
{
    margin: 20px 5px 10px 5px;
    padding: 20px 0;
}
```

<!-- v -->

## Bordures

```css
{
    border: 1px solid red;
}
```

`border: <width> <style> <couleur>`

### Un seul côté

```css
{
    border-bottom: 1px solid red;
}
```

<!-- v -->

### Largeur, style et couleur

```css
{
    border-width: 1px;
    border-style: solid;
    border-color: red;
}
```

### Annuler une bordure

```css
{
    border: none;
    border-right: none;
}
```

<!-- v -->

## Block arrondi

```css
{
    border-radius: 20px;
}
```

![Block arrondi](./assets/images/block-rounded.png)

<!-- v -->

## Définir un fond

```css
{
    background-color: #CCC;
    background-image: url('lien/vers/image/');
    background-repeat: repeat; /* no-repeat */
    background-position: top left; /* center | value px|% */
    background-size: 200px 100%; /* cover | contain… */
}
```

### En une ligne

Ou presque

```css
{
    background: #CCC url('image') no-repeat top left;
}
```

<!-- v -->

### Position de l'image de fond


background-position: `<haut du block> <gauche du block>`;

background-position-x: `<gauche du block>`;

background-position-y: `<haut du block>`;

<!-- v -->

### Taille de l'image de fond

`background-size: <largeur> <hauteur>;`

#### Mots-clé

* `cover` : l'image occupe, "couvre" tout l'espace disponible
* `contain`: l'image est entièrement visible, "contenue" dans l'espace disponible

<!-- s -->

<!-- .slide: data-background="#333" -->

# TP → Mise en forme <small>suite</small>

## Page

* Appliquer des marges et espacement aux sections :
    - marges `20px` en haut en bas
    - espacement `15px` à gauche et à droite
* Appliquer la couleur `#fff7f0` pour le fond de tout le contenu

<!-- v -->

<!-- .slide: data-background="#333" -->

## Pied de page

* Intégrer le pied de page pour le contact et les compétences
    - couleur texte #F8F8F8
    - couleur de fond #222222
* Appliquer un espacement entre la bordure et le texte sur le pied de page

<!-- s -->

<!-- .slide: data-background="#333" -->

# Responsive

<!-- v -->

## Media-queries

```css
@media screen and (min-width: 768px) and (orientation: portrait) {
    /* les styles pour cette configuration */
}
```

* Orientation (`portrait` ou `landscape`) et Localisation
* Device api (`screen`, `print`, `tv`…)

Importance de définir des points d'arrêt pertinents en fonction des terminaux les plus utilisés.

<!-- v -->

## Organisation et contenu responsive

* Organisation responsive : disposition varie en fonction de la définition d'écran
* Contenu responsive : certains contenus peuvent être affichés ou non selon la définition d'écran

<!-- v -->

## Approche mobile first

```css
sidebar {
    padding: 15px;
}
@media (min-width: 700px) {
    sidebar {
        flex-basis: 33%;
    }
}
@media (min-width: 1040px) {
    sidebar {
        flex-basis: 25%;
    }
}
```

<!-- s -->

<!-- .slide: data-background="#333" -->

# Positionnement / flux HTML

<!-- v -->

## Par défaut : static

L'élément reste dans le flux HTML

<!-- v -->

## Absolute

```css
{
    position: absolute;
}
```

![Positionnement absolu](./assets/images/position-absolute-window.png)

L'élément sort du flux

<!-- v -->

### Définir la position

```css
{
    position: absolute;
    top: 30px;
    left: 50%;
}
```

Position relatif à l'élément parent le plus proche positionné en relatif, ou au document par défaut

<!-- v -->

#### Aucun parent avec un positionnement relatif

![](./assets/images/position-absolute-window.png)

<!-- v -->

#### Le bloc gris a un positionnement relatif

![](./assets/images/position-absolute-container.png)


<!-- v -->

## Relative

```css
{
    position: relative;
    top: 10%;
    left: 50px;
}
```

* L'élément reste du flux
* Position relatif au document ou à l'élément parent le plus proche positionné en relatif

<!-- v -->

## Fixed

```css
{
    position: fixed;
    top: 10%;
    right: 20px;
}
```

* L'élément sort du flux
* Positionnement fixé par rapport à la partie visible du navigateur

<!-- s -->

<!-- .slide: data-background="#333" -->

# Mise en page

Disposition des éléments dans la page

<!-- v -->

## Élément flottant

```html
<img class="illustration-left"
    src="images/illu.png" />
<p>Lorem ipsum…</p>
```

```css
.illustration-left {
    float: left; /* right | none */
}
```

![Float left](./assets/images/float-image-left.png)

<!-- v -->

### Réinitialiser les float

Pour que le bloc

```css
h2 {
    clear: both; /* left | right */
}
```

![Clear both](./assets/images/float-clear.png)

<!-- v -->

### Float pour la disposition de la barre latérale

```html
<div id="sidebar">
</div>
<div id="main">
</div>
```

```css
#sidebar {
    width: 33%;
    float: right;
}
#main {
    width: 76%;
    float: left;
}
```

<!-- v -->

## Flexbox

Disponible dans CSS3, compatible tous navigateurs à jour

<!-- v -->

### Disposition de la barre latérale

```html
<div class="flex-wrapper">
    <div id="sidebar">
    </div>
    <div id="content">
    </div>
</div>
```

```css
.flex-wrapper {
    display: flex;
}
#sidebar {
    flex-basis: 33%;
}
#content {
    flex-basis: 76%;
}
```

En détail : [Toutes les directives flex](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

*Plus de CSS Flexbox et CSS Grids → CSS avancées*

<!-- s -->

<!-- .slide: data-background="#333" -->

# TP → Mise en page

* Section compétences à gauche 
* Photo de profil alignée à droite
* Positionner le footer en fixe

<!-- s -->

<!-- .slide: data-background="#333" -->

# Un peu de méthodologie

<!-- v -->

## Procéder par étape

1. organiser les éléments tels qu'ils doivent apparaître sur mobile, "Mobile first"
1. travailler sur les typographies (titres, paragraphes, listes) et couleurs
1. logo, header ou bannière
1. footer
1. menu, sidebar
1. disposer les éléments pour l'affichage page
1. page d'accueil
1. pages intérieures

<!-- s -->

<!-- .slide: data-background="#333" -->

# Bonus

<!-- v -->

## D'autres directives CSS

[css3generator.com](https://css3generator.com/)

<!-- v -->

## Textes et images de test

### Textes : Lorem Ipsum

Tout sur le [Lorem Ipsum](https://loremipsum.io/fr/)

* Générateur avancé [LorIpsum](https://loripsum.net/)
* [d'autres plus drôles](https://meettheipsums.com/)
    * [Zombieipsum](http://www.zombieipsum.com/)
    * [Samuel L. Ipsum](https://slipsum.com/)

<!-- v -->

### Images de test

* [Picsum Photos](https://picsum.photos/)
* [Lorem Pixel](https://lorempixel.com/)
* [d'autres plus drôles](https://loremipsum.io/fr/21-of-the-best-placeholder-image-generators/)

<!-- v -->

## Ressources graphiques libres

### Polices de caractères

- https://www.fontsquirrel.com

<!-- v -->

### Images

- [Recherche Creative Commons](https://search.creativecommons.org/)
- [Flickr](flickr.com/) → filtrer sur les licences droit de modification et réutilisation commerciale

<!-- v -->

### Logiciels de graphisme

- Gimp : très bon en retouche photo
- Inkscape : le meilleur logiciel de dessin vectoriel du monde
- Krita : ouvre bien les fichiers photoshop, permet de mêler vectoriel et raster

<!-- v -->

## Des jeux pour chez vous

- un jeu sur les sélecteurs [flukeout.github.io](http://flukeout.github.io/)
- un jeu sur flexbox : [flexboxfroggy](http://flexboxfroggy.com/#fr)
- un jeu sur grid : [CSS grid garden](http://cssgridgarden.com/#fr)

<!-- s -->

<!-- .slide: data-background="#333" -->

# Quizz

<!-- v -->

## Que veut dire CSS ?

a. Cascading Style Sheets

b. Cassoulet Sans Saucisse

c. Classe Styling Suite

<!-- v -->

## Où peut-on mettre du style ?

<!-- v -->

## Où peut-on mettre du style ?

- dans l'attribut `style=` d'une balise
- entre les balises `<style> </style>` dans le document html
- dans un fichier séparé

<!-- v -->

## Pour appeler un fichier séparé ?

```html
<link rel="stylesheet" type="text/css"
href="https://monsite.com/monfichier.css">
```

```html
<link rel="icon" type="image/png"
href="https://monsite.com/favicon.png">
```

```html
<meta name="viewport"
content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
```

<!-- v -->

## C'est quoi les deux modèles de boite en CSS ?

a. une boite qui contient un chat, soit vivant, soit mort

b. une boite qui fait soit "meuh", soit "bêêê"

c. une boite dont les dimensions totales sont calculées soit en comptant l'espacement et les marges (`content-box`), soit à partir des dimensions définies seulement (`border-box`)

<!-- v -->

## Quel(s) élement(s) sera(seront) sur fond orange avec `div.warning { backgroud-color: orange }`

a. les éléments de classe `warning` contenu dans un élément `div`

b. les éléments `div` et avec une classe `warning`

c. les éléments avec une classe `warning`

<!-- v -->

## Comment changer le style à partir de 1024px de large ?

<!-- v -->

## Comment changer le style à partir de 1024px de large ?

```css
@media (min-width: 1024px) {
    /* les styles pour cette configuration */
}
```

<!-- v -->

## Merci :)
