---
title: HTML
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

<!-- .slide: data-background="#333" -->

# HTML : structurer le Web

<!-- v -->

# Documentation

* officielle [w3c.org/TR/html5](http://www.w3.org/TR/html5/)
* compréhensible [developer.mozilla.org](https://developer.mozilla.org/fr/)

<!-- s -->

<!-- .slide: data-background="#333" -->

# HTML

<!-- v -->

## Sommaire

* Le navigateur et les outils
* Syntaxe du HTML
    * Structurer son texte
    * Listes
    * Tableaux
    * Media (figure, audio, video, SVG)
    * Formulaires
* Faire son CV en HTML

<!-- s -->

<!-- .slide: data-background="#333" -->

# Introduction

<!-- v -->

## Historique

* 1989 : début des travaux de *Tim Berners Lee*, chercheur au CERN, et son équipe. Le principe, l'hypertexte.
    **Protocole HTTP / Location URL / Language HTML**
* 1991 : *Tim Berners Lee* rend le projet **WorldWideWeb** public
* 1993 : création du navigateur **Mosaic**
* 1994 : création du **W3C** par *Tim Berners Lee*

<!-- v -->

* 1996 : implémentation de **CSS1.0** par **Internet Explorer**
* 1998 : Premières directives sur l'accessibilité **WCAG1** (Web Content Accessibility Guidelines)
* 1998 : publication des spécifications **CSS 2.0**

<!-- v -->

* 1999 : spécifications **HTML4.01**, et début des travaux sur **CSS3**
* 2000 : publication des spécifications pour **XHTML1**
* 2006 : publication des recommandations de **XHTML2.0**

Scission au sein du W3C : le **WHATWG** (Web Hypertext Application Technology Working Group) travaille sur un autre standard pour le **HTML**

<!-- v -->

* 2008 : "First Public Working Draft" du **HTML5** présentée par le **WHATWG**
* 2008 : Avancée sur l'accessibilité, **WCAG2.0**, Web Content Accessibility Guidelines
* 2012 : **HTML5** passe en "Candidate Recommandation"
* fin 2014 : **HTML5** est un standard du W3C
* Depuis 2012 : différentes parties de CSS3 passent en "candidate recommandation (CR)", puis en recommandation officielle (REC)
* 2018 : version 2.1 des **WCAG**

<!-- v -->

## Pourquoi les CSS ?

Séparer le fond (contenu) et la forme (habillage)

[CSS Zen Garden](http://www.csszengarden.com/), par Dave Shea

Pour un même code HTML, des thèmes multiples

<!-- v -->

## Les standards du Web et le W3C

* Le [W3C](https://www.w3.org) : organisme qui élabore les [standards](https://www.w3.org/standards/) des technologies liées au Web
* Constitué de membres de plusieurs organisations (Mozilla, Google, Microsoft, Intel…)
* Organisé en groupes de travail et d'intérêt (HTML, CSS, Accessibilité…)
* Processus d'élaboration des standards bien défini

En un mot : produit les spécifications qui seront implémentées par les navigateurs et autres outils

<!-- s -->

<!-- .slide: data-background="#333" -->

# Les outils

<!-- v -->

## Outils de développement

* Éditeur de code libre : [VS Codium](https://vscodium.com/)
* Navigateur : Firefox ou Chrome / l'inspecteur de code
* Test et validation
    * [validator.w3.org](http://validator.w3.org/) respect des standards
    * [opquast.com](http://opquast.com/fr/) pour la qualité du code, l'accessibilité
* Compatibilité navigateurs [caniuse.com](https://www.caniuse.com/)

Notes:

Visiter le site [access42.net](https://access42.net) et ouvrez l'inspecteur de code.

<!-- v -->

## Documentation

* Spécifications [standards HTML & CSS sur le W3C](https://www.w3.org/standards/webdesign/htmlcss)
* Documentation compréhensible [developer.mozilla.org](https://developer.mozilla.org/fr/)

<!-- s -->

# Activité

Le débuggeur du navigateur

Firefox et Chrome / Chromium
<!-- s -->

# Syntaxe du HTML

Basé sur `XML`

```html
<balise attribut="valeur">Contenu de la balise</balise>
```

Balise de paragraphe

```html
<p id="description">Ma description</p>
```

* p : élément (balise)
* id : attribut

<!-- v -->

## Structure

- DocType : `<!DOCTYPE html>`

- Entêtes :
  - Encodage : `<meta charset="UTF-8">`
  - Titre : `<title>Example page</title>`

- Body : `<body>`

<!-- v -->

## Exemple 1 : Pour commencer

```html
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Example page</title>
    </head>
    <body>
        <h1>Exemple de page</h1>
        <h2>Titre niveau 2</h2>
        <p>Example paragraph</p>
    </body>
</html>
```

<!-- s -->

<!-- .slide: data-background="#333" -->

# Organiser son texte

<!-- v -->

## Paragraphe et liens

```html
<p>Pour plus d'information visiter
    <a href="http://makina-corpus.com/">Makina Corpus</a>
</p>
```

## Entêtes

Six niveaux de titre `<h1>` à `<h6>`

<!-- v -->

## Insérer une image

```html
<img src="/chemin/vers/monimage.png"
    alt="Texte de remplacement" />
```

<!-- v -->

## Listes

```html
<ul>
    <li>Item 1</li>
    <li>Item 2</li>
</ul>
```

* Liste non ordonnées `<ul>`
* Liste ordonnées `<ol>`
* Liste de définition `<dl>`

```html
<dl>
    <dt>Terme</dt>
    <dd>Définition du terme</dd>
</dl>
```

<!-- v -->

## Tables

```html
<table class="listing">
    <thead>
        <tr>
            <th class="nom">Nom</th>
            <th class="prenom">Prénom</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2"> test test </td>
        </tr>
        <tr>
            <td>test </td>
            <td>test </td>
        </tr>
    </tbody>
</table>
```

<!-- v -->

# Comportements des balises

Balises bloc, balises en ligne

## Balises `inline`

`<a>`, `<em>`, `<strong>`, …

* les unes à côté des autres dans le flux
* pas de dimension de boite ou de marge possible
* balise générique : `<span>`

<!-- v -->

## Balises en `block`

`<p>`, `<h1>`, `<blockquotes>`, …

* les unes sous les autres dans le flux
* possibilité de définir une taille et des marges
* balise générique : `<div>`

## Autres comportements

`table`, `table-row`, `table-cell`, `flex`, `grid`…

<!-- s -->

<!-- .slide: data-background="#333" -->

# Media et éléments embarqués

<!-- v -->

## `<embed>`

* Pour embarquer une application externe (un plugin par exemple).
* Déjà existants en HTML4 :

    * `<object>`
    * `<iframe>` : Peut embarquer un autre site, un éditeur de texte riche ou une carte par exemple.

<!-- v -->

## [`<audio>` et `<video>`](https://developer.mozilla.org/fr/docs/Web/HTML/Utilisation_d%27audio_et_video_en_HTML5)

```html
<audio src="./donjon-crom.mp3" controls></audio>
<video src="video.ogg" controls
    poster="video.jpg" width="640" height="480">
```

On peut inclure plusieurs formats de media

```html
<video controls poster="video.jpg" width="640" height="480">
    <source src="video.ogg" />
    <source src="video.avi" />
    <source src="video.mp4" />
</video>
```

<!-- v -->

### Formats audio et vidéo / navigateurs

* ogg -> Chrome, Firefox, Edge, Opera
* webm -> tous sauf Internet Explorer
* MPEG-4/H.264 → tous les navigateurs
* MP3 → tous les navigateurs

Voir sur [CanIUse](http://caniuse.com/#search=video%20format)

<!-- v -->

## Image, attributs `srcset` et `sizes`

`src` est ignoré pour les user-agent supportant srcset.

```html
<img src="small.jpg"
    srcset="large.jpg 1024w, medium.jpg 640w, small.jpg 320w"
    sizes="(min-width: 360px) 33.3vw, (min-width: 980px) 980px, 100vw"
    alt="A rad wolf">
```

<!-- v -->

`sizes`

* Si (min-width: 360px) alors image de largeur 33.3% de la taille de la fenêtre
* Si (min-width: 980px) alors l'image de largeur 980px de la taille de la fenêtre
* sinon 100% de la largeur de la fenêtre

<!-- v -->

`srcset` propose plusieurs fichiers correspondant à des largeurs différente.
Le navigateur choisit le fichier en fonction de la taille de l'emplacement de l'image'.

Fonctionne avec Chrome, et firefox à partir de la version 38.

Liens utiles : [Balise img sur la doc Mozilla](https://developer.mozilla.org/fr/docs/Web/HTML/Element/Img), [Responsive image community group](http://responsiveimages.org/)


<!-- v -->

## Conteneur `<figure>`

Permet d'illustrer et ajouter une légende à une image, un schéma.

```html
<figure>
    <img src="image.jpg"
    alt="Texte alternatif" />
    <figcaption>Légende de l'image</figcaption>
</figure>
```

Peut contenir autre chose que des images : du code ou une vidéo par exemple.

<!-- v -->

## SVG

Dessiner en 2D vectorielle via XML

* Accès aux éléments d'un SVG depuis le DOM
* CSS applicables
* Peut être chargé depuis un fichier externe ou en ligne dans un document HTML
* L’arbre des données est conservé en mémoire

<!-- v -->

### Example

```html
<svg>
    <circle id="circle1" cx="40" cy="40" r="24" />
</svg>
```

Voir aussi [css-tricks.com/using-svg](http://css-tricks.com/using-svg/)

<!-- v -->

## Exercice: bannière différente selon la définition d'écran

* Insérez une bannière de taille différente selon la définition d'écran en utilisant srcset.
* Insérez un logo en SVG

<!-- s -->

<!-- .slide: data-background="#333" -->

# Formulaires

<!-- v -->

## Structure

```html
<form action="/page-de-traitement" method="get">
    <label>Nom</label>
    <input type="text" name="name" />
    <input type="submit" name="Valider" />
</form>
```

Deux méthodes : GET ou POST

<!-- v -->

## Types de champs

### Text

```html
<input type="text" />
```

Sur plusieurs lignes

```html
<textarea cols="30" rows="10"> < /textarea>
```

<!-- v -->

### Liste déroulante

```html
<select>
    <option>Banane</option>
    <option>Cerise</option>
    <option>Citron</option>
</select>
```

`<select multiple>` → liste choix multiple

<!-- v -->

### Bouton radio

```html
<input
    type="radio"
    checked
    id="soup"
    name="meal">
```

### Case à cocher

```html
<input type="checkbox"
    checked id="carrots"
    name="carrots"
    value="carrots">
```

<!-- v -->

## Contrôles `<input>` HTML5

`<input>` de type text, mais aussi :

* tel, url, email, search
* date, time, number, range
* color

<!-- v -->

## Attributs HTML5

* placeholder
* pattern
* autocomplete
* min, max, step (pour date, time, number et range)
* list

<!-- v -->

## Code

Url avec placeholder

```html
<input type="url" name="url" placeholder="Votre site Web" />
```

Range

```html
<input type="range" name="range"
    min="10" max="100" step="5" value="15"/>
```

Pattern

```html
<input type="text" name="pattern" pattern="[a-z]{2}[0-9]{2}" />
```

Liste

```html
<input type="text" name="ville" list="villes"/>
<datalist id="villes">
    <option value="Albi">
    <option value="Cahors">
</datalist>
```

<!-- s -->

<!-- .slide: data-background="#333" -->

## Exercice

Créez un formulaire avec les champs suivants :

* Nom complet,
* Téléphone,
* Date de naissance,
* Couleur préférée,
* Ville (parmi quelques villes)

Utilisez Chrome (ou Chromium) et Firefox pour afficher ce formulaire.

[Exemple complet](https://github.com/numahell/html5-css3/blob/master/html/forms.html)

<!-- s -->

<!-- .slide: data-background="#333" -->

# Notions de sémantique

<!-- v -->

## Balises

* Inline : `<mark>`, `<time>`, `<meter>`, `<progress>`
* [Section](https://developer.mozilla.org/fr/docs/Web/HTML/Sections_and_Outlines_of_an_HTML5_document) : `<section>`, `<article>`, `<main>`, `<aside>`, `<nav>`, `<header>`, `<footer>`
* Grouper : `<figure>`, `<figcaption>`

Code plus clair, page mieux structurée sémantiquement : un meilleur référencement.

<!-- Voir [tinytypo.tetue.net](http://tinytypo.tetue.net/tinytypo.html) -->

<!-- s -->

<!-- .slide: data-background="#333" -->

## Exemple 2 : Structure sémantique d'une page

```html
<body>
    <header>
        <nav data-role="menu">
            <ul>
            …
            </ul>
        </nav>
    </header>
    <main role="main">
        <article>
            <section>
                <h1>Éléphants de forêt</h1>
                <p>Dans cette section, nous discutons des éléphants de forêt moins connus.
                    Ce paragraphe continue…</p>
                <section>
                <h2>Habitat</h2>
                <p>Les éléphants de forêt ne vivent pas dans les arbres mais parmi eux.
                    Ce paragraphe continue…</p>
                <figure><img src="__mon__url__"
                    />
                </figure>
                <p>Texte</p>
                </section>
            </section>
        </article>
        <aside>
        … barre latérale …
        </aside>
    </main>
    <footer>
    …
    </footer>
</body>
```

Notes:

`header` et `footer` désignent l'entête et le pied de page, mais ne sont pas forcément en haut ou en bas de la page.
Peuvent être à l'intérieur d'un article.

<!-- s -->

<!-- .slide: data-background="#333" -->

# Dessin et animations

<!-- v -->

## CANVAS

* Surface de pixels contrôlés en JavaScript, API disponible
* Fonctionnement en "boite noire" : le **"Paint" du web**

<!-- v -->

## WebGL

Prend de l'ampleur, mais non encore complètement [supporté par tous les navigateurs](http://rando.ecrins-parcnational.fr/fr/boucle-du-pigeonnier-dans-le-cirque-du-gioberney) ni les drivers vidéos.

* De très belles expérimentations sur [Chrome Experience](https://www.chromeexperiments.com/webgl)
* Affichage 3D de randonnées dans l'application [Geotrek](http://geotrek.fr/), à voir sur [Rando Écrins](http://rando.ecrins-parcnational.fr/fr/boucle-du-pigeonnier-dans-le-cirque-du-gioberney)
* Voir aussi [Les interfaces de demain](http://fr.slideshare.net/makinacorpus/petit-djeuner-html5-et-css3-les-interfaces-de-demain) pour plus de détails


<!-- s -->

# Quizz

<!-- v -->

## balises, attribut ?

<!-- v -->

## Commencer une page ?

<!-- v -->

## Commencer une page ?

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" 
        content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
</body>
</html>
```

<!-- v -->

## Titres et des paragraphes ?

<!-- v -->

## Titres et des paragraphes ?

```html
<h1>Un beau titre</h1>
<p>mon paragraphe</p>
```
<!-- v -->

## Insérer un lien ?
`
<!-- v -->

## Insérer un lien ?

```html
<a href="https://wiki.formation-fullstack.fr/">
```
<!-- v -->

## Insérer une image ?

<!-- v -->

## Insérer une image ?

```html
<img src="https://placekitten.com/100/100">
```

<!-- v -->

## Liste numérotée ?

<!-- v -->

## Liste numérotée ?

```html
<ol>
    <li>item 1</li>
    <li>item 2</li>
</ol>
```

<!-- v -->

## `<div>` et `<span>`

<!-- v -->

## `<div>` et `<span>`

Balises génériques pour les blocs et les éléments en ligne.

<!-- v -->

## Résumé HTML

(fait par JC)

https://wiki.formation-fullstack.fr/minimal/html
