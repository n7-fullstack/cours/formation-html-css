---
title: CSS
author: Emmanuelle Helly
theme: solarized
---

# SASS

*Support inspiré de ce tuto*

[Reprenez le contrôle de vos feuilles de styles avec SASS](https://zestedesavoir.com/tutoriels/672/reprenez-le-controle-de-vos-feuilles-de-style-avec-sass/les-bases-de-sass/sass-un-preprocesseur-css/)

*CC-By-SA par Abelazer et Matouche*

----

## Sommaire

- Imbrication
- Variables
- Mixins
- Fonction
- Plus loin

---

# Pour commencer

----

## Sass, préprocesseur CSS

- génère du CSS à partir de fichiers .scss
- permet d’optimiser son code
- syntaxe : SCSS
- on peut inclure du CSS simple

----

## TP

Proposition: TP du tuto de zeste de savoirs

https://github.com/Matouche/CitronInc

----

## Installer

Avec npm (le plus simple)

```
npm init
npm install --save-dev node-sass
```

## Générer les styles

Créer un fichier `sass/main.scss`

```bash
node-sass -w sass/main.scss css/styles.css
```

----

## Arborescence

```bash
.
├── css
    └── styles.css
├── img
├── index.html
└── sass
    └── main.scss
```

---

# Imbrication

----

## Exemple

En CSS

```css
ul.menu {
    padding: 0;
}
ul.menu li {
    display: inline-block;
}
```

En SASS

```scss
ul.menu {
    padding: 0;
    li {
        display: inline-block;
    }
}
```

----

Mais aussi

```scss
ul {
    &.menu {
        padding: 0;
        li {
            display: inline-block;
        }
    }
}
```
----

## Imbrication de mediaqueries

```scss
ul.menu {
    padding: 0;
    li {
        display: inline-block;
        @media (max-width: 479px) {
            display: block;
        }
    }
}
```
----

## Attention

Éviter de cumuler trop d'imbrication, spécifie trop les sélecteurs

---

# Variables

----

## Ajouter une variable

```scss
$primary-color: #233d90;

h1 {
    color: $primary-color;
    border-bottom: 1px solid $primary-color;
}
```

----

## Calculs

```scss
$spacing: 20px;

h1 {
    margin-bottom: $spacing * 2;
}
```

```scss
$head-font: "Roboto";

h1, h2 {
    font-family: $head-font;
}
```

----

## Chaine de caractères

On stocke la règle dans une variable

```scss
$large-screen: "min-width: 740px";
```

Insertion en tant que chaîne de caractère

```scss
#description {
    max-width: 22rem;
    @media screen and ($large-screen) {
        max-width: 48%;
    }
}
```

----

## Interpolation

```scss
$small-screen: "screen and (max-width: 540px)";
$large-screen: "screen and (min-width: 740px)";
```

Ce code ne fonctionne pas :

```scss
@media $large-screen { … }
```

Syntaxe de l'interpolation

```scss
@media #{$large-screen} { … }
```

---

# Imports

----

```bash
sass
  ├─ _header.scss
  ├─ _button.scss
  ├─ _cards.scss
  ├─ _reset.scss
  ├─ _typo.scss
  └─ main.scss
```

```scss
@import 'header';
/* … */
```

---

# Mixins

----

## Comme des fonctions

Déclarer une mixin

```scss
@mixin button {
    // …
}
```

Insérer une mixin

```scss
.more {
    @include button;
}
```

----

## Passage de paramètres

```scss
@mixin button ($color, $background-color, $hover-color) {
    color: $color;
    // …
}
```

Appel de la mixin

```scss
.more {
    @include button(#3300FF, #EFEFEF, $dark-hover);
}
```

---

# Fonctions

----

## Couleurs

- rgb($red, $green, $blue)
- invert($color) : La couleur inverse (négatif)
- grayscale($color) : La nuance de gris correspondant à la couleur
- lighten($color, $amount), La couleur plus claire (selon le %)
…

----

## Construire sa fonction

```scss
@function largeur-grille($n, $colonne, $gouttiere){
  @return $n * $colonne + ($n - 1) * $gouttiere;
}

div{
  width: largeur-grille(12, 60px, 20px); //940px
}
```
