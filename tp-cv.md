# TP CV

## HTML

1. Créer une nouvelle page
2. Ajouter un titre : votre nom (`<h1>`)
3. Ajouter un paragraphe : votre métier (`<p>`)
4. Ajouter créez les sections : votre expérience, votre formation, vos compétences
5. Ajouter une section footer contact avec votre email

Une section

```html
<section class="xp">
    <h2>Mon expérience</h2>
    <ul>
        <li>2009 - 2013 : Développeuse web, Makina Corpus
    </ul>
</section>
```

## CSS

1. Couleur et bordure sur le titre
2. Couleur de fond sombre et texte clair sur la section compétences
3. Police sans-serif
4. Couleur de fond sombre et texte clair au survol d'un lien
5. Mise en page : marges et espacement sur chaque section
6. Bloc compétences aligné à gauche

